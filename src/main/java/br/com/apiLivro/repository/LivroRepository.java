package br.com.apiLivro.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import br.com.apiLivro.model.Livro;


@Repository
public interface LivroRepository extends CrudRepository<Livro, Long> {

}
