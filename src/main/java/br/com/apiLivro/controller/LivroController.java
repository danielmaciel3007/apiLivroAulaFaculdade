package br.com.apiLivro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.apiLivro.DTO.LivroDTO;
import br.com.apiLivro.service.LivroService;

@RestController
@RequestMapping(value = "/livro")
public class LivroController {

	@Autowired
	private LivroService livroService;
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ResponseEntity<LivroDTO> findLivroById(@PathVariable Long id) {
		LivroDTO LivroDTO = livroService.findById(id);
		if (LivroDTO != null) {
			return new ResponseEntity<LivroDTO>(LivroDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<LivroDTO>(LivroDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="", method = RequestMethod.GET)
	public ResponseEntity<List<LivroDTO>> findAll() {
		List<LivroDTO> LivroDTO = livroService.getLivros();
		if (LivroDTO != null) {
			return new ResponseEntity<List<LivroDTO>>(LivroDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<List<LivroDTO>>(LivroDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public ResponseEntity<LivroDTO> addLivro(@RequestBody LivroDTO LivroDTO) {
		LivroDTO = livroService.addLivro(LivroDTO);
		if (LivroDTO != null) {
			return new ResponseEntity<LivroDTO>(LivroDTO, HttpStatus.OK);
			
		} else {
			return new ResponseEntity<LivroDTO>(LivroDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/edit", method = RequestMethod.PUT)
	public ResponseEntity<LivroDTO> editLivro(@RequestBody LivroDTO LivroDTO) {
		LivroDTO = livroService.editLivro(LivroDTO);
		if (LivroDTO != null) {
			return new ResponseEntity<LivroDTO>(LivroDTO, HttpStatus.OK);
			
		} else {
			return new ResponseEntity<LivroDTO>(LivroDTO, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/delete", method = RequestMethod.DELETE)
	public ResponseEntity deleteLivro(@RequestBody LivroDTO LivroDTO) {
		try{
			livroService.deleteLivro(LivroDTO);
			return new ResponseEntity(HttpStatus.OK);
		} catch(Exception ex) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
	}
	
}
