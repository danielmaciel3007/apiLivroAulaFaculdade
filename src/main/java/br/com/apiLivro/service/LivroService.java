package br.com.apiLivro.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.apiLivro.DTO.LivroDTO;
import br.com.apiLivro.model.Livro;
import br.com.apiLivro.repository.LivroRepository;

@Service
public class LivroService {
	
	@Autowired
	private LivroRepository livroRepository;

	public LivroDTO findById(Long id) {
		Optional<Livro> oLivro = livroRepository.findById(id);
		if (oLivro != null && oLivro.isPresent()) {
			Livro livro = oLivro.get();
			LivroDTO livroDTO = new LivroDTO();
			
			livroDTO.setId(livro.getId());
			livroDTO.setTitulo(livro.getTitulo());
			livroDTO.setEditora(livro.getEditora());
			livroDTO.setAutor(livro.getAutor());
			
			return livroDTO;
		} else {
			return null;
		}
		
	}
	
	public List<LivroDTO> getLivros() {
		Iterable<Livro> iLivros = livroRepository.findAll();
		List<LivroDTO> ls = new ArrayList<LivroDTO>();
		
		for(Livro livro : iLivros) {
			
			LivroDTO livroDTO = new LivroDTO();
			
			livroDTO.setId(livro.getId());
			livroDTO.setTitulo(livro.getTitulo());
			livroDTO.setEditora(livro.getEditora());
			livroDTO.setAutor(livro.getAutor());
			
			ls.add(livroDTO);
		}
		
		return ls;
	}
	
	public LivroDTO addLivro(LivroDTO livroDTO) {
		Livro livro = new Livro();
		
		livro.setTitulo(livroDTO.getTitulo());
		livro.setEditora(livroDTO.getEditora());
		livro.setAutor(livroDTO.getAutor());
		
		livro = livroRepository.save(livro);
		livroDTO.setId(livro.getId());
		return livroDTO;
	}
	
	public LivroDTO editLivro(LivroDTO livroDTO) {
		Livro livro = new Livro();
		livro.setId(livroDTO.getId());
		livro.setTitulo(livroDTO.getTitulo());
		livro.setEditora(livroDTO.getEditora());
		livro.setAutor(livroDTO.getAutor());
		livro = livroRepository.save(livro);
		return livroDTO;
	}
	
	public void deleteLivro(LivroDTO livroDTO) {
		Livro livro = new Livro();
		livro.setId(livroDTO.getId());
		livro.setTitulo(livroDTO.getTitulo());
		livro.setEditora(livroDTO.getEditora());
		livro.setAutor(livroDTO.getAutor());
		livroRepository.delete(livro);
	}
	
}
